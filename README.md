# Please read this part

Hey guys,

I used my ASDJSEnvironment to build this project, my asd... is just a repo that I created around 2 years ago to help me start building things, it's just a set of configurations that I always like to use on my projects.

You can check it [here](https://github.com/reinaldoferreira/ASDJSEnvironment)

You can check the working project [here](http://asd-lazy-load.surge.sh/)

## Travis & Surge
I like `github` a lot and for this reason, you'll find a `.travis.yml` in this project, I would normally code this on `Github` but I didn't want to make this repo public and I didn't want to pay `Github` to make it private.

Basically, with my `ASDJSEnvirontmen` whenever something gets pushed to master, it triggers a build on `Travis` and if all of the tests are passing, then it deploys the static project to surge.sh

## What could be better
After doing some research on the lazy load topic, I found some cool topics, like the Facebook’s 200 byte technique, where I could generate smaller images and use them as placeholders with a blur effect (something I may do in the future, just for fun).

# ASD Nearly there yet?

ASD is short for Awesome Super Duper =D

## Getting started

Clone the repo

```
git clone git@github.com:reinaldoferreira/ASDJSEnvironment.git your-project-name
```

Inside of the project's folder (`cd your-project-name`), install all dependencies:

```
npm i
```

## Commands

The `start` command watches `js` and `sass` files for changes and to run tests and opens your project at `localhost:3000/`.

```
npm start
```

The `nsp` command checks if is there any insecure package installed on the project

```
npm run nsp
```

The `build` command builds the static files into `./dist`.

```
npm run build
```

The `deploy` command deploys the static files inside of `./dist` to surge.

```
npm run deploy
```

### CSS

The generated `css` file is loaded, compressed and cached through `webpack`.

The `scss` command builds the `main.min.scss` file inside `./src` stripping out comments and minifying the output.

```
npm run scss
```

The `scss:watch` command watches `scss` files for changes and builds the `main.min.scss` file inside `./src`.

```
npm run scss:watch
```

### Testing

```
npm test
```

```
npm test:watch
```

### Linting

```
npm run lint
```

```
npm run lint:watch
```
