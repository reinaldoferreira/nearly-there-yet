import '../css/main.min.css'
import { on, toggleClass, isInTheViewPort } from './utils/dom'
import { loadImage, loadArrayOfImages } from './utils/image-loader'

window.onload = () => {
  const articles = document.getElementsByClassName('lazy-load')
  const arrayOfArticles = Array.from(articles)
  let timer // to debounce the scroll

  const onScroll = () => {
    timer = timer || setTimeout(() => {
      timer = null
      loadArrayOfImages(arrayOfArticles)
    }, 300)
  }

  on('#results', 'click', (e) => {
    e.stopPropagation()
    const target = e.target

    if (target.classList.contains('main-button')) {
      const extraText = target.parentNode.querySelector('.extra-text')

      toggleClass(extraText, 'is-hidden')
      target.innerHTML = extraText.classList.contains('is-hidden') ? 'Show More' : 'Show Less'
    }
  })

  loadArrayOfImages(arrayOfArticles)
  on(document, 'scroll', onScroll)
}
