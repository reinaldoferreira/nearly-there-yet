/* global expect */
import { loadImage, loadArrayOfImages } from './image-loader'

describe('loadImage()', () => {
  it('Should add an image as background-image to a given element', () => {
    const element = document.createElement('span')
    element.setAttribute('data-src', 'path.to.image')

    expect(element.style.backgroundImage).toBe('')
    loadImage(element)
    expect(element.style.backgroundImage).toBe('url(path.to.image)')
  })

  it('Should remove the data-src attribute', () => {
    const element = document.createElement('span')
    element.setAttribute('data-src', 'path.to.image')

    expect(element.getAttribute('data-src')).toBe('path.to.image')
    loadImage(element)
    expect(element.getAttribute('data-src')).toBe(null)
  })
})

