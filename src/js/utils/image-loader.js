import { isInTheViewPort } from './dom'

export const loadImage = (element) => {
  if (element) {
    const imagePath = element.getAttribute('data-src')

    if (imagePath) {
      element.style.backgroundImage = `url('${imagePath}')`
      element.removeAttribute('data-src')
    }
  }
}

export const loadArrayOfImages = (arrayOfElements) => {
  if (arrayOfElements && Array.isArray(arrayOfElements)) {
    arrayOfElements.forEach(element => {
      if (isInTheViewPort(element)) {
        loadImage(element)
      }
    })
  }
}
