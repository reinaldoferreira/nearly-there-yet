/* global expect */
/* global jest */
import { isObject, getElement, on, toggleClass, removeClass, addClass, isInTheViewPort } from './dom'

describe('isObject()', () => {
  test('it should check if the given value is an object or not', () => {
    expect(isObject({ name: 'Bob', surname: 'Burns' })).toBeTruthy()
    expect(isObject(null)).toBeTruthy()
    expect(isObject(['Hulk', 'Lola'])).toBeFalsy()
    expect(isObject('I am a string')).toBeFalsy()
    expect(isObject(222)).toBeFalsy()
  })
})

describe('getElement()', () => {
  it('Should return a DOM element', () => {
    // Mocked HTML elements
    document.body.innerHTML = `
      <p class="p-class">Sweet class</p>
      <div id="divId">Sweet id</div>
      <button>Sweet button</button>
    `

    expect(getElement('.p-class')).toBe(document.querySelectorAll('.p-class')[0])
    expect(getElement('.p-class').innerHTML).toBe('Sweet class')
    expect(getElement('#divId')).toBe(document.querySelectorAll('#divId')[0])
    expect(getElement('#divId').innerHTML).toBe('Sweet id')
    expect(getElement('button')).toBe(document.querySelectorAll('button')[0])
    expect(getElement('button').innerHTML).toBe('Sweet button')
  })
})

describe('toggleClass()', () => {
  test('it should toggle a class of a given element', () => {
    document.body.innerHTML = '<p class="p-class">Some text</p>'
    // Store the element to be tested
    let test = document.querySelectorAll('.p-class')[0]

    // Add the 'is-active' class
    toggleClass('.p-class', 'is-active')
    expect(test.classList[1]).toBe('is-active')

    // Remove the 'is-active' class
    toggleClass('.p-class', 'is-active')
    expect(test.classList[0]).toBe('p-class')
    expect(test.classList[1]).toBeUndefined()
  })
})

describe('removeClass()', () => {
  test('it should remove the given class from the the given element', () => {
    document.body.innerHTML = `
      <p class="p-class extra-class">Some text</p>
      <p class="p-class extra-class">Some text 2</p>
      <button class="submit-btn extra-class">Submit</button>
      <button class="cancel-btn extra-class">Cancel</button>
      <div class="some-div extra-class"></div>
    `

    let arr = ['.submit-btn', '.cancel-btn']
    let obj = document.getElementsByClassName('p-class')
    let el = document.getElementsByClassName('some-div')

    expect(el[0].classList).toContain('extra-class')
    removeClass(el, 'extra-class')
    expect(el[0].classList).not.toContain('extra-class')

    let itemInArray = document.querySelectorAll(arr[0])[0]
    expect(itemInArray.classList).toContain('extra-class')
    removeClass(arr, 'extra-class')
    expect(itemInArray.classList).not.toContain('extra-class')

    expect(obj[0].classList).toContain('extra-class')
    removeClass(obj, 'extra-class')
    expect(obj[0].classList).not.toContain('extra-class')
  })
})

describe('addClass()', () => {
  test('it should add the given class from the the given element', () => {
    document.body.innerHTML = `
      <p class="p-class">Some text</p>
      <p class="p-class">Some text 2</p>
      <button class="submit-btn">Submit</button>
      <button class="cancel-btn">Cancel</button>
      <div class="some-div"></div>
    `

    let arr = ['.submit-btn', '.cancel-btn']
    let obj = document.getElementsByClassName('p-class')
    let el = document.getElementsByClassName('some-div')

    expect(el[0].classList).not.toContain('extra-class')
    addClass(el, 'extra-class')
    expect(el[0].classList).toContain('extra-class')

    let itemInArray = document.querySelectorAll(arr[0])[0]
    expect(itemInArray.classList).not.toContain('extra-class')
    addClass(arr, 'extra-class')
    expect(itemInArray.classList).toContain('extra-class')

    expect(obj[0].classList).not.toContain('extra-class')
    addClass(obj, 'extra-class')
    expect(obj[0].classList).toContain('extra-class')
  })
})

describe('on()', () => {
  test('it should add the given eventListener to the given element', () => {
    document.body.innerHTML = `
      <button class="add-first">Add First</button>
      <button class="add-second">Add Second</button>
    `

    on('.add-first', 'click', () => document.body.innerHTML += '<p>Hulk</p>')

    expect(document.body.innerHTML).not.toContain('Hulk')
    document.getElementsByClassName('add-first')[0].click()
    expect(document.body.innerHTML).toContain('Hulk')

    document.body.innerHTML = `
      <button class="add-first">Add First</button>
      <button class="add-second">Add Second</button>
    `

    on(['.add-first', '.add-second'], 'click', () => document.body.innerHTML += '<p>Hulk and Lola</p>')
    expect(document.body.innerHTML).not.toContain('Hulk and Lola')
    document.getElementsByClassName('add-second')[0].click()
    expect(document.body.innerHTML).toContain('Hulk and Lola')
  })
})

// needs improvement
describe('isInTheViewPort()', () => {
  beforeEach(() => {
    Element.prototype.getBoundingClientRect = jest.fn(() => {
      return {
        width: 120,
        height: 120,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      }
    })
  })

  it('Should mock `getBoundingClientRect`', () => {
    const element = document.createElement('span');
    const rect = element.getBoundingClientRect();
    expect(rect.width).toEqual(120);
  });

  it('Should check if the given element is present in the viewport', () => {
    const element = document.createElement('span');
    expect(isInTheViewPort(element)).toBeTruthy()
  })
})

